#Test Ansible

ansible-playbook get-pdi-project-deployer-playbook.yml -e project_path=deploy_example -e project_name=ansible_example -e tag=[branch name]

ansible-playbook deploy-pdi-project-playbook.yml --tags deploy_apps,repo_download -e "deploy_product_name=deploy_example/myapp product_version=main deploy_folder=app_dir"
